# Module Image : A simple system to interact with images
This module implements two classes, Pixel and Image.
An image is an array of Pixels.

We are using the SDL2 to handle the Window and the display.
If you're using the window, you might need to use T and G to zoom and dezoom the Image.

We are using .ppm files to import and export all image files.
Some documentation may be found [here](https://en.wikipedia.org/wiki/Netpbm)

## Authors
Ethan PEGEOT <ethan.pegeot@etu.univ-lyon1.fr>
Maxence BANEGAS <maxence.banegas@etu.univ-lyon1.fr>

# How to build
Please note that this project will only link on Linux due to the inclusion of the SDL2 Headers for Linux.
All behaviour on other platform is considered unspecified and not tested.

`make` will build the three different files, one that just generates two images into the data directory (`bin/exemple`), one that will create a window to display an example (`bin/affichage`), and another that will run the regression tests on the Image class (`bin/test`)

# How to generate documentation

We are using Doxygen to generate the documentation, which outputs a whole load of html files into the `doc/html` directory, those files are static and may be displayed using any recent browser.
`doxygen && firefox doc/html/index.html`