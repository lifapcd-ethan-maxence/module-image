#ifndef IMAGE_H
#define IMAGE_H

# include "Pixel.h"
# include <string>

# include <SDL2/SDL.h>
# include <SDL2/SDL_image.h>

using std::string;
/**
 * \brief The Image class, which represents a 2D array of Pixels
 * \author Ethan PEGEOT <p2105678@etu.univ-lyon1.fr>
 * 
 * This class implements an Image in it's most basic sense : a 2D array of pixels
 * The first pixel is located at the unsigned coordinates (0,0), the last one at (dimx-1, dimy-1)
*/
class Image
{
    private:
        unsigned int dimx; /**< The x dimension of the image */
        unsigned int dimy; /**< The y dimension of the image*/
        Pixel* tab; /**< An array of pixels, must be allocated in the heap*/
        SDL_Window * window; /**< The SDL window, used by afficher and it's private methods*/
        SDL_Renderer * renderer; /**< The SDL renderer*/
        SDL_Surface * image_surface; /**< One of the variables representing the Image in the SDL World*/
        SDL_Texture * image_texture; /**< One of the variables representing the Image in the SDL World*/
        /**
         * \var export_filename The path where to export the image in order to reimport it from the SDL
         * \note The folder hierarcgy leading to the file needs to be created (eg. data/ needs to exist)
        */
        const char * export_filename = "data/TEMP_EXPORT.ppm";
        const int x_size = 200; /**< The x size of the window used to display the image*/
        const int y_size = 200; /**< The y size of the window used to display the image*/

        /**
         * SDL SECTION
        */

        /**
         * \brief Creates a SDL_Windows, inits all the variables related to SDL in the class.
        */ 
        void sdl_init();

        /**
         * \brief Loads the image stored at path `export_filename` to the SDL_Texture/Surface
        */
        void sdl_load();

        /**
         * \brief This function handles the event & render loops
        */ 
        void sdl_image() const;

        /**
         * \brief Frees the memory related to the SDL, destroys the window.
        */ 
        void sdl_quit() const;

    public:
        /**
         * \brief The empty constructor of the Image class
         * This constructor does nothing, it creates an empty image, which will need to be resized before use.
        */
        Image();
        /**
         * \brief This constructors creates an image of the given dimensions
         * \param dimenxionX The size of the image on the x unsigned coordinate (the length of a line)
         * \param dimensionY The size of the image on the y unsigned coordinate (the height of the image)
         * 
         * This constructor creates an image made of black pixels.
        */
        Image(const unsigned int dimensionX, const unsigned int dimensionY);
        ~Image();
        // Getters 
        /**
         * \brief This function returns a reference to the pixel at unsigned coordinates (x,y)
         * \param x The position of the pixel in the x unsigned coordinate; This must be inferior to dimx
         * \param y The position of the pixel in the y unsigned coordinate; This must be inferior to dimy
         * \return Pixel&
        */
        Pixel& getPix(const unsigned int x, const unsigned int y) const; // La moulinette veut un const entre ) et ;

        // Setters
        /**
         * \brief This functions sets the value of the (x,y)th pixel to the value provided by couleur
         * \param x The x unsigned coordinate, must be inferior to dimx
         * \param y The y unsigned coordinate, must be inferior to dimy
         * \param couleur The Pixel you want to set in the image
         */
        void setPix(const unsigned int x, const unsigned int y, const Pixel& couleur);

        /**
         * \brief This functions fill the rectangle represented by the 4 given unsigned coordinates with the couleur Pixel
         * \param Xmin The x unsigned coordinate of the up-left corner of the rectangle
         * \param Ymin The y unsigned coordinate of the up-left corner of the rectangle
         * \param Xmax The x unsigned coordinate of the down-right corner of the rectangle
         * \param Ymax The y unsigned coordinate of the down-right corner of the rectangle
         * \param couleur The pixel you want to fill in the rectangle
         * 
         * This functions fills from (xmin,ymin) to (xmax, ymax) INCLUDED the Pixel couleur
        */
        void dessinerRectangle(const unsigned int Xmin, 
                               const unsigned int Ymin, 
                               const unsigned int Xmax, 
                               const unsigned int Ymax, 
                               const Pixel couleur);
        /**
         * \brief This function resets all the Pixels on the image with couleur
         * \param couleur The pixel used to fill all the image
        */
        void effacer(const Pixel couleur);

        /**
         * \brief The function which performs the regression tests
         * \author Maxence BANEGAS <p2101340@etu.univ-lyon1.fr>
         * 
         * This function creates two images, one which is empty, the other one that is from a designated size, and tries
         * to use the main functions on it.
        */
        void testRegression() const;

        // File handlers
        /**
         * \brief Saves the Image to a .ppm file
         * \param filename A string representing the file path
        */
        void sauver(const string & filename) const;
        /**
         * \brief Loads an image from a .ppm file
          * \param filename A string representing the file path
         */
        void ouvrir(const string & filename);

        /**
         * \brief This method outputs to the standard output the content of the Image
         * The first line is made of the x and y coordinates, sperated by a space
         * The other lines represent each R,G,B components of each pixel, in order from top-left to bottom-right
        */
        void afficherConsole() const;

        /**
         * \brief This method creates a SDL Window in which is displayed the Image, use T and G to zoom/dezoom
        */
        void afficher();
};  

#endif  