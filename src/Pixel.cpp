#include "Pixel.h"
#include <assert.h>

Pixel::Pixel()
: r(0), g(0), b(0)
{}

Pixel::Pixel(const unsigned char nr, const unsigned char ng, const unsigned char nb)
: r(nr), g(ng), b(nb) 
{}

Pixel::~Pixel() {}

unsigned char Pixel::getRouge() const
{
   return this->r; 
}

unsigned char Pixel::getVert() const
{
    return this->g;
}

unsigned char Pixel::getBleu() const
{
    return this->b;
}

void Pixel::setRouge(const unsigned char nr)
{
    assert (0<nr && nr<255);
    this->r=nr;
}

void Pixel::setVert(const unsigned char ng)
{
    assert (0<ng && ng<255);
    this->g=ng;
}

void Pixel::setBleu(const unsigned char nb)
{
    assert (0<nb && nb<255);
    this->b=nb;
}
