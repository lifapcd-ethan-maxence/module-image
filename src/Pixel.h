#ifndef PIXEL_H
# define PIXEL_H

/**
 * \brief The Pixel class, which represents a unsigned char with red, green and blue components
 * \author Maxence BANEGAS <p1211340@etu.univ-lyon1.fr>
 * 
 * This class represents a Pixel/colour, this colour has red, green and blue components which can be set using all the getters & setters.
 * Please note that when you try to cout the colours, you need to cast them to int else you'll have some weird chars
*/
class Pixel
{
    private:
        unsigned char r, g, b; /**< The three components of colour, red, green, blue*/
    
    public:
        /**
         * \brief The empty constructor, which creates a black pixel
         * 
         * This constructor creates a pixel with each component set to 0
        */
        Pixel();
        /**
         * \brief This is the constructor of the class which sets the colour to the wanted values
         * \param nr An unsigned char [0;255] which represents the red component
         * \param ng An unsigned char [0;255] which represents the green component
         * \param nb An unsigned char [0;255] which represents the blue component
        */
        Pixel(const unsigned char nr, const unsigned char ng, const unsigned char nb);

        ~Pixel();

        // Getters
        /**
         * \brief The getter for the red component
         * \return The unsigned char representing the red component
        */
        unsigned char getRouge() const;
        /**
         * \brief The getter for the green component
         * \return The unsigned char representing the green component
        */
        unsigned char getVert() const;
        /**
         * \brief The getter for the blue component
         * \return The unsigned char representing the blue component
        */
        unsigned char getBleu() const;

        // Setters
        /**
         * \brief Sets the red component of the pixel to nr
         * \param nr The red component to be set in the pixel
        */
        void setRouge(const unsigned char nr);
        /**
         * \brief Sets the green component of the pixel to ng
         * \param ng The green component to be set in the pixel
        */
        void setVert(const unsigned char ng);
        /**
         * \brief Sets the blue component of the pixel to nr
         * \param nb The blue component to be set in the pixel
        */
        void setBleu(const unsigned char nb);
};
#endif