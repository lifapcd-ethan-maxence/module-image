#include "Image.h"
#include "Pixel.h"
#include <assert.h>

#include <iostream>
#include <fstream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

Image::Image() : dimx(0), dimy(0), tab(nullptr) {}

Image::Image(const unsigned int dimensionX, const unsigned int dimensionY) 
: dimx(dimensionX), dimy(dimensionY)
{
    this->tab = new Pixel[dimensionX*dimensionY];
}

Image::~Image()
{
    if (this->tab != nullptr)
        delete [] this->tab;
    this->tab = nullptr;
}

void Image::effacer(Pixel couleur)
{
    this->dessinerRectangle(0,0, this->dimx - 1, this->dimy - 1, couleur);
}

Pixel& Image::getPix(const unsigned int x, const unsigned int y) const
{
    assert((x <= this->dimx) && (y <= this->dimy));
    return this->tab[y*this->dimx+x];
}

void Image::setPix(const unsigned int x, const unsigned int y, const Pixel &couleur)
{
    assert((x <= this->dimx) && (y <= this->dimy));
    this->tab[y*this->dimx+x] = couleur;
}

void Image::dessinerRectangle(const unsigned int Xmin,
                              const unsigned int Ymin,
                              const unsigned int Xmax,
                              const unsigned int Ymax,
                              Pixel couleur)
{
    // We don't need to assert unsigned int_max for 0 bc unsigned int_max > unsigned int_min > 0 
    assert((Xmin <= Xmax && Ymin <= Ymax) && (Xmax < this->dimx && Ymax < this->dimy));
    for (unsigned int i = Xmin; i <= Xmax; i++)
    {
        for (unsigned int j = Ymin; j <= Ymax; j++)
        {
            this->setPix(i, j, couleur);
        }
    }
}

void Image::testRegression() const
{
    Image a;
    assert(a.dimx==0 && a.dimy==0&& a.tab==nullptr);
    Image b(100,50);
    assert(b.dimx==100 && b.dimy==50 && b.tab!=nullptr);
    Pixel c(150,150,150);
    b.dessinerRectangle(0,0,50,25,c);
    for(unsigned int x=0;x<100;x++)
    {
        for(unsigned int y=0;y<50;y++)
        {
            Pixel pix = b.getPix(x,y);
            if(x<=50 && y<=25)
            {
                assert(pix.getRouge() ==150 && pix.getVert() == 150 && pix.getBleu() == 150  );
            }
            else
            {
                assert(pix.getRouge() ==0 && pix.getVert() == 0 && pix.getBleu() == 0  );
            }
            
        }
    }
    Pixel couleur;
    b.setPix(10,5,couleur);
    Pixel p= b.getPix(10,5);
    assert(p.getRouge() == 0 && p.getVert() == 0 && p.getBleu() == 0);
    Pixel color(200,200,200);
    b.effacer(color);
    for(int i=0;i<100;i++)
    {
        for(int j=0;j<50;j++)
        {
            Pixel pix=b.getPix(i,j);
            assert(pix.getRouge() == 200 && pix.getVert() == 200 && pix.getBleu() == 200);
        }
    }
}

using namespace std;
void Image::sauver(const string & filename) const 
{
    ofstream fichier (filename.c_str());
    assert(fichier.is_open());
    fichier << "P3" << endl;
    fichier << dimx << " " << dimy << endl;
    fichier << "255" << endl;
    for(unsigned int y=0; y<dimy; y++)
        for(unsigned int x=0; x<dimx; x++) {
            Pixel& pix = getPix(x,y);
            fichier << (int) pix.getRouge() << " " <<  (int) pix.getVert() << " " << (int) pix.getBleu() << endl; // rgb en private // caster en (int)
        }
    cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}

void Image::ouvrir(const string & filename) 
{
    ifstream fichier (filename.c_str());
    assert(fichier.is_open());
	unsigned char r,g,b;
	string mot;
	dimx = dimy = 0;
	fichier >> mot >> dimx >> dimy >> mot;
	assert(dimx > 0 && dimy > 0);
	if (this->tab != NULL) delete [] this->tab;
	this->tab = new Pixel [dimx*dimy];
    for(unsigned int y=0; y<dimy; ++y)
        for(unsigned int x=0; x<dimx; ++x) {
            fichier >> r >> b >> g;
            getPix(x,y).setRouge(r);
            getPix(x,y).setVert(g);
            getPix(x,y).setBleu(b);
        }
    fichier.close();
    cout << "Lecture de l'image " << filename << " ... OK\n";
}

void Image::afficherConsole() const
{
    cout << dimx << " " << dimy << endl;
    for(unsigned int y=0; y<dimy; y++) {
        for(unsigned int x=0; x<dimx; x++) {
            Pixel& pix = getPix(x,y);
            cout << (int) pix.getRouge() << " " << (int) pix.getVert() << " " << (int) pix.getBleu() << endl;
        }
        cout << endl;
    }
}

void Image::sdl_init()
{
   if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        cout << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << endl;SDL_Quit();exit(1);
    }
    
    int imgFlags = IMG_INIT_PNG | IMG_INIT_JPG;
    if( !(IMG_Init(imgFlags) & imgFlags)) {
        cout << "SDL_m_image could not initialize! SDL_m_image Error: " << IMG_GetError() << endl;SDL_Quit();exit(1);
    }
    
    window = SDL_CreateWindow("Module Image", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, x_size, y_size, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    if (window == NULL) {
        cout << "Erreur lors de la creation de la fenetre : " << SDL_GetError() << endl; SDL_Quit(); exit(1);
    }

    renderer = SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED); 
}

void Image::sdl_load()
{
    this->sauver(export_filename);

    image_surface = IMG_Load(export_filename);
    if (image_surface == nullptr)
    {
        cout << "Error: cannot load " << export_filename << endl;
        exit(1);
    }

    SDL_Surface *surfaceCorrectPixelFormat = SDL_ConvertSurfaceFormat(image_surface, SDL_PIXELFORMAT_ARGB8888, 0);
    SDL_FreeSurface(image_surface);
    image_surface = surfaceCorrectPixelFormat;

    image_texture = SDL_CreateTextureFromSurface(renderer, surfaceCorrectPixelFormat);
    if (image_texture == nullptr)
    {
        cout << "Error: problem to create the texture of " << export_filename << endl;
        exit(1);
    }
}

void Image::sdl_image() const
{
    SDL_Event events;
	bool quit = false;

    float zoom_ratio = 10.0;
	while (!quit) {
		while (SDL_PollEvent(&events)) { // Event loop
			if (events.type == SDL_QUIT) quit = true;           // Si l'utilisateur a clique sur la croix de fermeture
			else if (events.type == SDL_KEYDOWN) 
            { 
				switch (events.key.keysym.scancode) 
                {
                    case SDL_SCANCODE_ESCAPE:
                    case SDL_SCANCODE_Q:
                        quit = true;
                        break;
                    case SDL_SCANCODE_G:
                        zoom_ratio = zoom_ratio - 1 > 1 ? zoom_ratio - 1 : 0;
                        break;
                    case SDL_SCANCODE_T:
                        zoom_ratio += 1.0;
                        break;
                    default: break;
				}
			}
		}

        // Render at each frame
		SDL_SetRenderDrawColor(renderer, 100, 100, 100, 255);
        SDL_RenderClear(renderer);
        
        int w = (this->dimx*zoom_ratio); int h = (this->dimy*zoom_ratio); // We will play on those coordinates to make the changes
        int x = (1/2.0)*(x_size-w); int y = (1/2.0)*(y_size-h); // We use these to center the image
        int ok;
        SDL_Rect r;
        r.x = x;
        r.y = y;
        r.w = (w<0)?image_surface->w:w;
        r.h = (h<0)?image_surface->h:h;

        ok = SDL_RenderCopy(renderer,image_texture,nullptr,&r);
        assert(ok == 0);
	    // on permute les deux buffers (cette fonction ne doit se faire qu'une seule fois dans la boucle)
        SDL_RenderPresent(renderer);
	}
}

void Image::sdl_quit() const
{
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}
void Image::afficher()
{
    sdl_init();
    sdl_load();
    sdl_image();
    sdl_quit();
}