SRCS		= Image.cpp Pixel.cpp
HEADERS		= src 
CXX			= g++
CXX_FLAGS	= -Wall -Werror -Wextra
LD			= g++
LD_FLAGS	= -lSDL2 -lSDL2_ttf -lSDL2_image

OBJ_DIR = obj
BIN_DIR = bin
SRC_DIR = src

AFFICHAGE_NAME	= affichage
TEST_NAME 		= test
EXEMPLE_NAME 	= exemple

AFFICHAGE_MAIN	= mainAffichage.cpp
TEST_MAIN		= mainTest.cpp
EXEMPLE_MAIN	= mainExemple.cpp
MAINS = mainAffichage.cpp mainTest.cpp mainExemple.cpp # Sers pour clean
OBJS = ${SRCS:%.cpp=$(OBJ_DIR)/%.o} # Sers aussi pour les cleans maintenant

all: mkdir affichage exemple test

# affichage builds bin/affichage, and it's the same for all the others
affichage: ${BIN_DIR}/${AFFICHAGE_NAME}
${BIN_DIR}/${AFFICHAGE_NAME}: ${OBJ_DIR}/mainAffichage.o ${OBJS} # Build for affichage
	${CXX} ${FLAGS} -o $@ $^ ${LD_FLAGS}

exemple: ${BIN_DIR}/${EXEMPLE_NAME}
${BIN_DIR}/${EXEMPLE_NAME}: ${OBJ_DIR}/mainExemple.o ${OBJS} # Build for affichage
	${CXX} ${FLAGS} -o $@ $^ ${LD_FLAGS}

test: ${BIN_DIR}/${TEST_NAME}
${BIN_DIR}/${TEST_NAME}: ${OBJ_DIR}/mainTest.o ${OBJS} # Build for affichage
	${CXX} ${FLAGS} -o $@ $^ ${LD_FLAGS}

${OBJ_DIR}/Pixel.o: ${SRC_DIR}/Pixel.cpp ${SRC_DIR}/Pixel.h
	${CXX} ${FLAGS} -c $< -o $@ -I ${HEADERS}

${OBJ_DIR}/Image.o: ${SRC_DIR}/Image.cpp ${SRC_DIR}/Image.h ${SRC_DIR}/Pixel.h
	${CXX} ${FLAGS} -c $< -o $@ -I ${HEADERS}

${OBJ_DIR}/mainAffichage.o: ${SRC_DIR}/mainAffichage.cpp ${SRC_DIR}/Image.h ${SRC_DIR}/Pixel.h
	${CXX} ${FLAGS} -c $< -o $@ -I ${HEADERS}

${OBJ_DIR}/mainExemple.o: ${SRC_DIR}/mainExemple.cpp ${SRC_DIR}/Image.h ${SRC_DIR}/Pixel.h
	${CXX} ${FLAGS} -c $< -o $@ -I ${HEADERS}

${OBJ_DIR}/mainTest.o: ${SRC_DIR}/mainTest.cpp ${SRC_DIR}/Image.h ${SRC_DIR}/Pixel.h
	${CXX} ${FLAGS} -c $< -o $@ -I ${HEADERS}

mkdir:
	mkdir -p $(BIN_DIR) $(OBJ_DIR) data

clean: fclean
	rm -f ${BIN_DIR}/${AFFICHAGE_NAME} ${BIN_DIR}/${TEST_NAME} ${BIN_DIR}/${EXEMPLE_NAME}

fclean: # fclean only removes the .o files, while clean removes the .o AND the binaries
	rm -f ${OBJS} ${MAINS:%.cpp=$(OBJ_DIR)/%.o}

ffclean:
	rm -f data/*.ppm

re: fclean all